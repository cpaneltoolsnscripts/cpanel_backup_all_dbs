---
description: "A simple bash script which allows you to rapidly dump all databases when using cPanel or MySQL Database user with priveleges provided."
---

# cPanel All Databases backup

A simple bash script which allows you to rapidly dump all databases when using cPanel or MySQL Database user with priveleges provided.

## Overview

Features:

- Autofetches all mysql databases for the current user to array and loops through to dump.
- Uses ~/.my.cnf to store mysql password for passwordless login.
- Just requires Bash which every cPanel should have by default.
- Very lightweight no external libraries required.


## Use cases
- This is aimed at power users/resellers who do not have root access and need an easy way to backup their databases.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Login via SSH and download the script to the account.

```
wget -O ${HOME}/cpanel_backup_all_dbs.sh https://gitlab.com/cpaneltoolsnscripts/cpanel_backup_all_dbs/raw/master/cpanel_backup_all_dbs.sh && chmod +x ${HOME}/cpanel_backup_all_dbs.sh
```

### Installing

Edit the script configure settings if desired. This will skip the interactive prompt for the mysql database/cPanel user and corresponding pass.


This can be done via nano or vi/vim.

```
nano ${HOME}/cpanel_backup_all_dbs.sh
```

The section will look like this. Set your information and backup directory preferences.

```
#######Start Configuring###############
#Set the cPanel login here or a db user with priveleges to all databases for backup purposes
DBUSER="username"
PASSWORD="password"

#Backup directory full or relative path
BACKUP_DIR="$HOME/backup"

#######Stop Configuring###############
```

Alternatively you can also run this and it will prompt you for the username and password and save to ~/.my.cnf. 

I recommend using the cPanel login as it already has all the permissions needed to access all the accounts current databases and future ones added. If that is not desired you can also setup a new mysql user for just making backups and grant the user privelegs to all the current databases. The only downside to this is any newly created databases will need the mysql backup user added after created.

```
bash ${HOME}/cpanel_backup_all_dbs.sh
```


Any time the script runs after being setup it will work without prompts as it relies on the ~/.my.cnf. So it can be run as often or as little as needed as all new files are output with timestamp to prevent conflict.

```
-rw-rw-r-- 1 username username 3.3M 01/03/20 username_blogcom-2020-01-03_08:32:03.gz
-rw-rw-r-- 1 username username 3.3M 01/03/20 username_blogcom-2020-01-03_08:33:57.gz
```

To see the list of databases this would backup the below one liner will list the databases.
```
declare -a dbarray; readarray -t dbarray < <(uapi Mysql list_databases |grep database| grep -v list_databases |sed 's/^[ \t]*//;s/[ \t]*$//'| sed 's/database: //g'| tr '/' '\n'); for DB in "${dbarray[@]}"; do echo ${DB};done
```

Example:
```
[username@cpanel ~]$ declare -a dbarray; readarray -t dbarray < <(uapi Mysql list_databases |grep database| grep -v list_databases |sed 's/^[ \t]*//;s/[ \t]*$//'| sed 's/database: //g'| tr '/' '\n'); for DB in "${dbarray[@]}"; do echo ${DB};done
username_blogcom
username_ocar899
username_test
username_wiz
username_wp630
username_wp688
username_wp76
username_wp949
[username@cpanel ~]$ 
```

## Example: Running the installer on second run or if script is configuration is set.


```
[username@cpanel ~]$ bash cpanel_backup_all_dbs.sh 
The /home/username/.my.cnf file exists. All good to proceed with passwordless.

Please note: To update user or passwords please update the /home/username/.my.cnf file
Starting Database backups...

Yessiree! This whole place would completely fall apart without old Claptrap keeping things humming along!

username_blogcom exported to /home/username/backup
username_ocar899 exported to /home/username/backup
username_test exported to /home/username/backup
username_wiz exported to /home/username/backup
username_wp630 exported to /home/username/backup
username_wp688 exported to /home/username/backup
username_wp76 exported to /home/username/backup
username_wp949 exported to /home/username/backup
Make sure to backup these files offsite.
-rw-rw-r-- 1 username username 3.3M 01/03/20 username_blogcom-2020-01-03_08:32:03.gz
-rw-rw-r-- 1 username username 3.3M 01/03/20 username_blogcom-2020-01-03_08:33:57.gz
-rw-rw-r-- 1 username username  90K 01/03/20 username_ocar899-2020-01-03_08:32:05.gz
-rw-rw-r-- 1 username username  90K 01/03/20 username_ocar899-2020-01-03_08:33:58.gz
-rw-rw-r-- 1 username username  445 01/03/20 username_test-2020-01-03_08:32:05.gz
-rw-rw-r-- 1 username username  446 01/03/20 username_test-2020-01-03_08:33:59.gz
-rw-rw-r-- 1 username username  51K 01/03/20 username_wiz-2020-01-03_08:32:05.gz
-rw-rw-r-- 1 username username  51K 01/03/20 username_wiz-2020-01-03_08:33:59.gz
-rw-rw-r-- 1 username username  21K 01/03/20 username_wp630-2020-01-03_08:32:05.gz
-rw-rw-r-- 1 username username  21K 01/03/20 username_wp630-2020-01-03_08:33:59.gz
-rw-rw-r-- 1 username username  12K 01/03/20 username_wp688-2020-01-03_08:32:05.gz
-rw-rw-r-- 1 username username  12K 01/03/20 username_wp688-2020-01-03_08:33:59.gz
-rw-rw-r-- 1 username username  59K 01/03/20 username_wp76-2020-01-03_08:32:05.gz
-rw-rw-r-- 1 username username  59K 01/03/20 username_wp76-2020-01-03_08:33:59.gz
-rw-rw-r-- 1 username username 311K 01/03/20 username_wp949-2020-01-03_08:32:05.gz
-rw-rw-r-- 1 username username 311K 01/03/20 username_wp949-2020-01-03_08:33:59.gz

Claptrap: Sure thing, sir! Aaaand OPEN! Have a lovely afternoon, and thank you for using Hyperion Robot Services. Let me know if you have any other portal-rific needs!
       ,
       |
    ]  |.-._
     \|"(0)"| _]
     `|=\#/=|\/
      :  _  :
       \/_\/ 
        |=| 
        `-'  
[username@cpanel ~]$
```

## Built With

* [BASH](https://www.gnu.org/software/bash/) - The scripting used


## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/mikeramsey/) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning
 

## Authors

* **Michael Ramsey** - *Initial work* - [mikeramsey](https://gitlab.com/mikeramsey/)

See also the list of [contributors](https://gitlab.com/cpaneltoolsnscripts/cpanel_backup_all_dbs/-/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration :

Password with stars
http://www.ict.griffith.edu.au/anthony/software/askpass_stars_v1.sh.txt

Claptrap ASCII art
https://www.reddit.com/r/ASCII/comments/1p81bk/art_claptrap/